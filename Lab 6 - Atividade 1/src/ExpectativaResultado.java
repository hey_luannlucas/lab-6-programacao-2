import java.util.Map;

public class ExpectativaResultado {
    public void exibirMedias(Map<String, Double> mediasPorRegiao) {
        System.out.println("Região\tMédia de Expectativa");
        System.out.println("---------------------------");

        for (Map.Entry<String, Double> entry : mediasPorRegiao.entrySet()) {
            System.out.printf("%s\t%.2f\n", entry.getKey(), entry.getValue());
        }
    }
}
