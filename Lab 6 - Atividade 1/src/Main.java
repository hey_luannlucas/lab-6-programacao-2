import javax.swing.*;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showOpenDialog(null);

        if (result == JFileChooser.APPROVE_OPTION) {
            java.io.File selectedFile = fileChooser.getSelectedFile();

            try {
                LerArquivo leitor = new LerArquivo();
                List<Regiao> dadosList = leitor.lerArquivo(selectedFile.getAbsolutePath());

                CalcularMedia calculadora = new CalcularMedia();
                Map<String, Double> mediasPorRegiao = calculadora.calcularMedias(dadosList);

                ExpectativaResultado exibidor = new ExpectativaResultado();
                exibidor.exibirMedias(mediasPorRegiao);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Nenhum arquivo selecionado.");
        }
    }
}
