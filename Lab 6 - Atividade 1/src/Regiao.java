public class Regiao {
    private String regiao;
    private double[] expectativas;

    public Regiao(String regiao, double[] expectativas) {
        this.regiao = regiao;
        this.expectativas = expectativas;
    }

    public String getRegiao() {
        return regiao;
    }

    public double[] getExpectativas() {
        return expectativas;
    }
}
