import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LerArquivo {
    public List<Regiao> lerArquivo(String nomeArquivo) throws IOException {
        List<Regiao> dados = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(nomeArquivo))) {
            String line;
            boolean firstLine = true;
            String[] anos = null;

            while ((line = br.readLine()) != null) {
                String[] parts = line.split(";");
                if (firstLine) {
                    anos = parts;
                    firstLine = false;
                } else {
                    String regiao = parts[0];
                    double[] expectativas = new double[anos.length - 1];

                    for (int i = 1; i < parts.length; i++) {
                        expectativas[i - 1] = Double.parseDouble(parts[i]);
                    }

                    Regiao dadosExpectativaVida = new Regiao(regiao, expectativas);
                    dados.add(dadosExpectativaVida);
                }
            }
        }

        return dados;
    }
}
