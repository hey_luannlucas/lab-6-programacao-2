import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalcularMedia {
    public Map<String, Double> calcularMedias(List<Regiao> dadosList) {
        Map<String, Double> mediasPorRegiao = new HashMap<>();

        for (Regiao dados : dadosList) {
            double[] expectativas = dados.getExpectativas();
            double media = calcularMedia(expectativas);
            mediasPorRegiao.put(dados.getRegiao(), media);
        }

        return mediasPorRegiao;
    }

    private double calcularMedia(double[] valores) {
        double soma = 0;
        for (double valor : valores) {
            soma += valor;
        }
        return soma / valores.length;
    }
}
