import java.util.Scanner;

public class Menu {
    private Agenda agenda;
    private Scanner scanner;

    public Menu() {
        agenda = new Agenda();
        scanner = new Scanner(System.in);
    }

    public void exibirMenu() {
        agenda.carregarContatos();

        int opcao;
        do {
            System.out.println("----- MENU -----");
            System.out.println("1. Adicionar um novo contato");
            System.out.println("2. Consultar um contato pelo nome");
            System.out.println("3. Remover um contato pelo nome");
            System.out.println("4. Encerrar o programa");
            System.out.print("Escolha uma opção: ");
            opcao = scanner.nextInt();
            scanner.nextLine(); // consumir a quebra de linha

            switch (opcao) {
                case 1:
                    adicionarContato();
                    break;
                case 2:
                    consultarContato();
                    break;
                case 3:
                    removerContato();
                    break;
                case 4:
                    System.out.println("Encerrando o programa...");
                    break;
                default:
                    System.out.println("Opção inválida.");
                    break;
            }
        } while (opcao != 4);
    }

    private void adicionarContato() {
        System.out.print("Digite o nome do contato: ");
        String nome = scanner.nextLine();
        System.out.print("Digite o telefone do contato: ");
        String telefone = scanner.nextLine();
        agenda.adicionarContato(nome, telefone);
        System.out.println("Contato adicionado com sucesso.");
    }

    private void consultarContato() {
        System.out.print("Digite o nome do contato: ");
        String nome = scanner.nextLine();
        agenda.consultarContato(nome);
    }

    private void removerContato() {
        System.out.print("Digite o nome do contato: ");
        String nome = scanner.nextLine();
        boolean contatoRemovido = agenda.removerContato(nome);
        if (contatoRemovido) {
            System.out.println("Contato removido com sucesso.");
        } else {
            System.out.println("Contato não encontrado.");
              }
          }
     }