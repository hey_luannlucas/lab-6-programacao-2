import java.io.*;
import java.util.*;

public class Agenda {
    private Map<String, Contato> contatos;

    public Agenda() {
        contatos = new HashMap<>();
    }

    public void carregarContatos() {
        try (BufferedReader reader = new BufferedReader(new FileReader("contatos.txt"))) {
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] dados = linha.split(",");
                String nome = dados[0];
                String telefone = dados[1];
                Contato contato = new Contato(nome, telefone);
                contatos.put(nome, contato);
            }
        } catch (IOException e) {
            System.out.println("Arquivo de contatos não encontrado. Criando arquivo... ");
        }
    }

    public void adicionarContato(String nome, String telefone) {
        Contato contato = new Contato(nome, telefone);
        contatos.put(nome, contato);
        salvarContatos();
    }

    public void consultarContato(String nome) {
        Contato contato = contatos.get(nome);
        if (contato != null) {
            System.out.println("Telefone do contato " + nome + ": " + contato.getTelefone());
        } else {
            System.out.println("Contato não encontrado.");
        }
    }

    public boolean removerContato(String nome) {
        if (contatos.containsKey(nome)) {
            contatos.remove(nome);
            salvarContatos();
            return true;
        } else {
            return false;
        }
    }
    public void salvarContatos() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("contatos.txt"))) {
            for (Contato contato : contatos.values()) {
                writer.write(contato.getNome() + "," + contato.getTelefone());
                writer.newLine();
            }
        } catch (IOException e) {
            System.out.println("Erro ao gravar o arquivo de contatos: " + e.getMessage());
        }
    }
}